<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Interfaces\NodeInterface;
use App\Repositories\NodeRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(NodeInterface::class, NodeRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
