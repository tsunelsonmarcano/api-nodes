<?php

namespace App\Helpers;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Models\Node;

class TransformHelper
{
    public function numberToLetters(int $number, string $lang = 'en')
    {
        $formatter = new \NumberFormatter($lang, \NumberFormatter::SPELLOUT);
        $text = $formatter->format($number);
        return $text;
    }

    public function transformOne(Node $node)
    {
        $node->parent = null;
        if (count($node->nodes)) {
            $node->parent = $node->nodes;
            $this->recursiveAssign($node->nodes);
        }
        return $node;
    }

    public function transformMultiple(Collection $collections)
    {
        $this->recursiveAssign($collections);
        return $collections;
    }

    public function recursiveAssign($data)
    {
        foreach ($data as $value) {
            if (isset($value->nodes)) {
                $value->parent = null;
                if (count($value->nodes)) {
                    $value->parent = $value->nodes;
                    $this->recursiveAssign($value->nodes);
                }
            }
        }
    }
}