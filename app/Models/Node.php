<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Carbon;

class Node extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'parent_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'parent_id',
        'nodes',
        'children',
        'updated_at',
    ];

    public function parents(): HasMany
    {
        return $this->hasMany(Node::class, 'id', 'parent_id');
    }

    public function nodes()
    {
        return $this->parents()->with('nodes');
    }


    public function children(): HasMany
    {
        return $this->hasMany(Node::class, 'parent_id', 'id');
    }

    public function nodesChildren()
    {
        return $this->children()->with('nodesChildren');
    }

    protected function createdAt(): Attribute
    {
        $request = request();

        if ($request->hasHeader('Timezone')) {
            $timezone = $request->header('Timezone');
            return Attribute::make(
                get: fn (string $value) => Carbon::parse($value)->tz($timezone)->format('Y-m-d H:i:s'),
            );
        }

        return Attribute::make(
            get: fn (string $value) => Carbon::parse($value)->format('Y-m-d H:i:s'),
        );
    }

    protected function title(): Attribute
    {
        $request = request();

        if ($request->hasHeader('Accept-Language')) {
            $lang = $request->header('Accept-Language');
            if ($lang !== 'en') {
                $formatter = new \NumberFormatter($lang, \NumberFormatter::SPELLOUT);
                return Attribute::make(
                    get: fn (string $value, array $attributes) => $formatter->format($attributes['id']),
                );
            }
        }

        return Attribute::make(
            get: fn (string $value) => $value,
        );
    }
}
