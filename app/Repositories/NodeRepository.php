<?php

namespace App\Repositories;

use App\Interfaces\NodeInterface;
use App\Models\Node;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Helpers\TransformHelper;
use Illuminate\Support\Facades\DB;

class NodeRepository implements NodeInterface
{
    private TransformHelper $transformHelper;
    private Node $model;
    private Collection $collection;

    /**
     * Create a new NodeController instance.
     *
     * @return void
     */
    public function __construct(TransformHelper $transformHelper)
    {
        $this->transformHelper = $transformHelper;
    }

    public function all(Request $request): NodeRepository
    {
        $nodes = Node::with('nodes')
            ->whereExists(function($query) {
                $query->select(DB::raw(1))
                    ->from(DB::raw('nodes AS a'))
                    ->whereRaw("a.parent_id = nodes.id");
            })->get();    
        $this->setCollection($nodes);
        return $this;
    }

    public function find(Node $model): NodeRepository
    {
        $model->load(['nodes', 'nodesChildren']);
        $this->setModel($model);
        return $this;
    }

    public function findMainChildren(Node $model): NodeRepository
    {
        $model->load(['nodes', 'children']);
        $this->setModel($model);
        return $this;
    }

    public function store(array $form): NodeRepository
    {
        $node = Node::create([
            'parent_id' => $form['parent'] ?? null,
        ]);

        $node->title = $this->transformHelper->numberToLetters((int)$node->id);
        $node->save();
        $node->load('nodes');
        $this->setModel($node);
        return $this;
    }

    public function update(Node $model, array $form): NodeRepository
    {
        $model->update([
            'parent_id' => $form['parent'] ?? $model->parent_id,
        ]);
        $model->load('nodes');
        $this->setModel($model);
        return $this;
    }

    public function destroy($model): void
    {
        $model->delete();
    }

    public function validateChildren($id)
    {
        $node = Node::where('id', $id)
            ->whereExists(function($query) {
                $query->select(DB::raw(1))
                    ->from(DB::raw('nodes AS a'))
                    ->whereRaw("a.parent_id = nodes.id")
                    ->limit(1);
            })->exists();
        return $node;
    }

    private function setModel(Node $model): void
    {
        $this->model = $model;
    }

    private function getModel(): Node
    {
        return $this->model;
    }

    private function setCollection(Collection $collection): void
    {
        $this->collections = $collection;
    }

    private function getCollection(): Collection
    {
        return $this->collections;
    }

    public function transformOne()
    {
        return $this->transformHelper->transformOne($this->getModel());
    }

    public function transformMultiple()
    {
        return $this->transformHelper->transformMultiple($this->getCollection());
    }
}