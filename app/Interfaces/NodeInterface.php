<?php

namespace App\Interfaces;
use App\Models\Node;
use Illuminate\Http\Request;

interface NodeInterface
{
    public function all(Request $request);
    public function find(Node $model);
    public function store(array $form);
    public function update(Node $model, array $form);
    public function destroy(Node $model);
}