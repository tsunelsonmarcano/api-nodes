<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNodeRequest;
use App\Http\Requests\UpdateNodeRequest;
use App\Http\Resources\NodeResource;
use App\Models\Node;
use Symfony\Component\HttpFoundation\Response;
use App\Interfaces\NodeInterface;
use Illuminate\Http\Request;

/**
 * @OA\Info(
 *    title="Api para manejar nodos",
 *    version="1.0.0",
 * )
 */
class NodeController extends Controller
{
    private NodeInterface $nodeRepository;

    /**
     * Create a new NodeController instance.
     *
     * @return void
     */
    public function __construct(NodeInterface $nodeRepository)
    {
        $this->nodeRepository = $nodeRepository;
    }

    /**
     * @OA\Get(
     *      path="/api/nodes",
     *      operationId="index",
     *      summary="Get list of parents nodes",
     *      description="Returns list of all parents node",
     *      @OA\Response(
     *          response=200,
     *          description="Ok"
     *       )
     *     )
     *
     * Returns list of projects
     */
    public function index(Request $request)
    {
        $nodes = $this->nodeRepository
            ->all($request)
            ->transformMultiple();
        return NodeResource::collection($nodes);
    }

     /**
     * @OA\Post(
     *     path="/api/nodes",
     *     operationId="store",
     *     summary="Add a new node",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="parent",
     *                     type="integer"
     *                 ),
     *                 example={"parent": 1}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={"data":{"created_at":"2024-01-22 00:55:37","id":101,"title":"one","parent":null}}, summary="An result object.")
     *         )
     *     )
     * )
     */
    public function store(StoreNodeRequest $request)
    {
        $validated = $request->validated();
        $validated = $request->safe()->all();
        $node = $this->nodeRepository
            ->store($validated)
            ->transformOne();
        return new NodeResource($node);
    }

    /**
     * @OA\Get(
     *     path="/api/nodes/{id}",
     *     operationId="show",
     *     summary="Get children from parent node",
     *     @OA\Parameter(
     *         description="Id of node",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         @OA\Examples(example="int", value="1", summary="An int value.")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok"
     *     )
     * )
     */
    public function show(Node $node, Request $request)
    {
        if ($request->has('depth') && $request->input('depth') == 1) {
            $model = $this->nodeRepository
                ->find($node)
                ->transformOne();
        } else {
            $model = $this->nodeRepository
                ->findMainChildren($node)
                ->transformOne();
            $model->nodes_children = $model->children;
        }
        return new NodeResource($model);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateNodeRequest $request, Node $node)
    {
        $validated = $request->validated();
        $validated = $request->safe()->all();
        $model = $this->nodeRepository
            ->update($node, $validated)
            ->transformOne();
        return new NodeResource($model);
    }

    /**
     * @OA\Delete(
     *     path="/api/nodes/{id}",
     *     operationId="destroy",
     *     summary="Remove a node",
     *     @OA\Parameter(
     *         description="Id of node",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         @OA\Examples(example="int", value="1", summary="An int value.")
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No Content"
     *     )
     * )
     */
    public function destroy(Node $node)
    {
        $exist = $this->nodeRepository->validateChildren($node->id);

        if ($exist) {
            return response()->json(['error' => 'You cannot delete when you have associated children'], Response::HTTP_BAD_REQUEST);
        }

        $this->nodeRepository->destroy($node);
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
