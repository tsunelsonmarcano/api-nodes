<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Node;

class NodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $limit = 100;
        $formatter = new \NumberFormatter('en', \NumberFormatter::SPELLOUT);

        for ($i = 1; $i <= $limit; $i++) { 
            $params = [
                'title' => $formatter->format($i),
            ];

            Node::create($params);
        }


        for ($i = 1; $i <= $limit; $i++) { 
            $node = Node::find($i);

            $parent = rand(1, $i);

            if ($parent === $node->id) {
                $parent = null;
            }

            $node->parent_id = $parent;
            $node->save();
        }
    }
}
