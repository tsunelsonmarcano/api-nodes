# Proyecto Laravel API

Este proyecto Laravel proporciona una API con operaciones CRUD básicas para gestionar nodos. A continuación, se describen los pasos para instalar y ejecutar el proyecto.

## Requisitos

Asegúrate de tener instaladas las siguientes herramientas en tu entorno de desarrollo:

- PHP 8.1
- MySQL 8.0
- Composer

## Pasos de instalación

1. Clona el repositorio:

    git clone git@gitlab.com:tsunelsonmarcano/api-nodes.git

2. Navega al directorio del proyecto:

    cd nombre_del_proyecto

3. Instala las dependencias del proyecto con Composer:

    composer install

4. Copia el archivo de configuración .env:

    cp .env.example .env

5. Configura las variables de entorno en el archivo .env según tu entorno de desarrollo y configuración de base de datos.

6. Ejecuta las migraciones para crear la estructura de la base de datos:

    php artisan migrate

7. Ejecuta los seeders para poblar la base de datos con datos de ejemplo:

    php artisan db:seed

## Ejecución del proyecto

1. php artisan serve

    El proyecto estará disponible en http://localhost:8000.

## Documentación Swagger
Puedes acceder a la documentación Swagger de la API en las siguientes rutas:

POST /api/nodes
GET /api/nodes
GET /api/nodes/{id}
DELETE /api/nodes/{id}
Abre tu navegador y visita http://localhost:8000/api/documentation para explorar la documentación interactiva.

¡Listo! Ahora deberías tener el proyecto Laravel configurado y en funcionamiento en tu entorno local.